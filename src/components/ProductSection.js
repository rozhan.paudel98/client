import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import Carousel from "react-elastic-carousel";
import Item from "../components/Itemforproducts";



export default function ProductSection({ productsItems, cartfunc }) {
  const history = useHistory();

  const products = productsItems ? productsItems : [];
  const [cartItems, setCartItems] = useState([]);
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  const breakPoints = [
    { width: 1, itemsToShow: 2 },
    { width: 550, itemsToShow: 3 },
    { width: 750, itemsToShow: 3 },
    { width: 1200, itemsToShow: 5 },
  ];

  //Component Did Mount
  useEffect(() => {
    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  return (
    <div>
      <h3 className="text-center">
        <img
          src="./featured-tag.jfif"
          style={{ height: "60px", width: "70px", marginRight: "10px" }}
        />
        Hot Products
        <img
          src="./featured-tag.jfif"
          style={{ height: "60px", width: "70px", marginLeft: "10px" }}
        />
      </h3>
      <Carousel breakPoints={breakPoints} itemsToShow={5}>
        {products.map((product, index) => {
          return (
            <>
              <Item>
                <div class="content__card">
                  <Link to={`/product?id=${product._id}`}>
                    <img src={product.image.toString()} />
                  </Link>
                  <h3
                    style={{
                      padding: "2px",
                      fontSize: "15px",
                      fontWeight: "900",
                    }}
                  >
                    {product.productName}
                  </h3>
                  {/* <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  </p> */}
                  <h6>
                    {currencyIndex == "EUR"
                      ? "EUR" + (product.price.productPrice * EUR).toFixed(4)
                      : currencyIndex == "AUD"
                      ? "AUD" + (product.price.productPrice * AUD).toFixed(4)
                      : "USD" + product.price.productPrice}
                  </h6>
                  <ul>
                    <li>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </li>
                    <li>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </li>
                    <li>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </li>
                    <li>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </li>
                    <li>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </li>
                  </ul>
                  <button class="buy-2" onClick={() => cartfunc(product)}>
                    Add to Cart
                  </button>
                </div>
              </Item>
            </>
          );
        })}
      </Carousel>
    </div>
  );
}

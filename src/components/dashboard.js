import axios from "axios";

import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { serverUrl } from "../utils/api";

export default function Dashboard() {
  const [tickets, setTickets] = useState([]);
  const [issue, setIssue] = useState("");
  const [details, setDetail] = useState("");
  const [productName, setProductName] = useState("");
  const [issuedBy, setIssuedBy] = useState("");
  const [isAdded, setIsAdded] = useState("false");
  const [userTrans, setUserTrans] = useState([]);
  const [user, setUser] = useState("User");

  useEffect(async () => {
    setUser(localStorage.getItem("email"));
    //fetching user tickets history
    axios
      .get(`${serverUrl}/api/ticket/fetch/tickets/user`, {
        headers: {
          "x-access-token": localStorage.getItem("token"),
        },
      })
      .then((result) => {
        console.log(result.data);
        setTickets(result.data.data);
      });
    //fetching transcations details
    axios
      .get(`${serverUrl}/api/transcations/fetch/user/transcations`, {
        headers: {
          "x-access-token": localStorage.getItem("token"),
        },
      })
      .then((result) => {
        setUserTrans(result.data.data);
      })
      .catch(() => {
        //handle error
      });
  }, []);

  useEffect(() => {
    console.log(localStorage.getItem("email"));
    axios
      .get(`${serverUrl}/api/ticket/fetch/tickets/user`, {
        headers: {
          "x-access-token": localStorage.getItem("token"),
        },
      })
      .then((result) => {
        console.log(result.data);

        var arr1 = result.data.data.reverse();
        setTickets(arr1);
      });
  }, [isAdded]);

  const handleOpenTicket = (e) => {
    e.preventDefault();
    const data = {
      issue,
      details,
      productName,
      issuedBy,
    };
    //sending data to backend
    axios
      .post(`${serverUrl}/api/ticket/create/ticket`, data, {
        headers: {
          "x-access-token": localStorage.getItem("token"),
        },
      })
      .then((result) => {
        console.log(result.data);
        toast.success(result.data.msg);
        if (result.data.isSuccess) {
          setIssue("");
          setIssuedBy("");
          setDetail("");
          setProductName("");
          setIsAdded(!isAdded);
        }
      });
  };

  return (
    <div>
      <ToastContainer />
      <section id="checkout">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="checkout-area">
                <h4>Welcome</h4>
                <p>{user}</p>
                <hr />
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <div
                    style={{
                      height: "100%",
                      flex: "0.5",
                    }}
                  >
                    <form className="form-group">
                      <h3>Ticket</h3>
                      <label htmlFor="issue_title">Enter Issue</label>
                      <input
                        className="form-control "
                        name="issue_title"
                        style={{ width: "80%" }}
                        placeholder="Enter Issue title"
                        id="issue_title"
                        onChange={(e) => setIssue(e.target.value)}
                        value={issue}
                      ></input>
                      <label htmlFor="issue_title">Details</label>
                      <textarea
                        className="form-control "
                        name="issue_title"
                        type="text-area"
                        style={{ width: "80%" }}
                        value={details}
                        placeholder="Enter details"
                        id="issue_title"
                        onChange={(e) => setDetail(e.target.value)}
                      ></textarea>
                      <label htmlFor="issue_title">Product Name</label>
                      <input
                        className="form-control "
                        name="issue_title"
                        style={{ width: "80%" }}
                        value={productName}
                        placeholder="Enter product name"
                        id="issue_title"
                        onChange={(e) => setProductName(e.target.value)}
                      ></input>
                      <label htmlFor="issue_title">Your Email</label>
                      <input
                        className="form-control "
                        name="issue_title"
                        style={{ width: "80%" }}
                        value={issuedBy}
                        placeholder="Please type your valid email"
                        id="issue_title"
                        onChange={(e) => setIssuedBy(e.target.value)}
                      ></input>
                      <button
                        className="btn btn-sm btn-secondary "
                        style={{ marginTop: "3px" }}
                        onClick={handleOpenTicket}
                      >
                        Open Ticket
                      </button>
                    </form>
                    <hr></hr>
                    <h3>Check your ticket Status</h3>
                    <div
                      style={{
                        overflowY: "scroll",

                        height: "250px",
                      }}
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">S.N</th>
                            <th scope="col">Issue</th>
                            <th scope="col">Details</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          {tickets
                            ? tickets.map((ticket, index) => {
                                return (
                                  <tr>
                                    <th scope="row">{index + 1}</th>
                                    <td>{ticket.issue}</td>
                                    <td>{ticket.details}</td>
                                    <td>{ticket.productName}</td>
                                    <td>
                                      {ticket.status == 1 ? "Open" : "Closed"}
                                    </td>
                                  </tr>
                                );
                              })
                            : null}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div
                    style={{
                      height: "300px",
                      flex: "0.5",
                      color: "black",
                    }}
                  >
                    <h3>Your overall transcations history</h3>
                    <hr></hr>
                    <table className="table">
                      <thead>
                        <tr>
                          <th scope="col">Products Names</th>
                          <th scope="col">Payment Method</th>
                          <th scope="col">Total Amount</th>
                          {/* <th scope="col">Description</th> */}
                        </tr>
                      </thead>
                      <tbody style={{ overflowY: "scroll" }}>
                        {userTrans.map((trans, index) => {
                          return (
                            <tr>
                              <th scope="row">{trans.description}</th>
                              <td>Paypal</td>
                              <td>{trans.amount.total}</td>
                              {/* <td>@mdo</td> */}
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

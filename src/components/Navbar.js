import React, { useState } from "react";
import "../css/customNavbar.css";
import {
  FaFacebookSquare,
  FaInstagramSquare,
  FaYoutubeSquare,
} from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";

import { Link, NavLink } from "react-router-dom";
import { NavDropdown } from "react-bootstrap";

const Navbar = () => {
  const [showMediaIcons, setShowMediaIcons] = useState(false);
  return (
    <>
      <nav className="main-nav">
        {/* 1st logo part  */}

        {/* 2nd menu part  */}
        <div
          className={
            showMediaIcons ? "menu-link mobile-menu-link" : "menu-link"
          }
        >
          <ul>
            <li>
              <NavLink to="/">Home</NavLink>
            </li>
            <li>
              <NavDropdown
                className="dropdownMenu"
                title="Category  &#9660;"
                id="basic-nav-dropdown"
              >
                <NavDropdown.Item className="dropdownItem">
                  {" "}
                  <Link to="/category?tag=ms-office"> MS-Ofice </Link>
                </NavDropdown.Item>
                <NavDropdown.Item className="dropdownItem">
                  <Link to="/category?tag=games"> Games </Link>{" "}
                </NavDropdown.Item>
                <NavDropdown.Item className="dropdownItem">
                  <Link to="/category?tag=others">Others </Link>{" "}
                </NavDropdown.Item>
                {/* <NavDropdown.Item className= "dropdownItem">ms-ofice </NavDropdown.Item> */}
              </NavDropdown>
            </li>
            <li>
              <NavLink to="/aboutus">About</NavLink>
            </li>
            <li>
              <NavLink to="/aboutus">Services</NavLink>
            </li>
            <li>
              <NavLink to="/contact">Contact</NavLink>
            </li>
          </ul>
        </div>

        {/* 3rd social media links */}
        <div className="social-media">
          <ul className="social-media-desktop">
            <li>
              <a href="#" target="">
                <FaFacebookSquare className="facebook" />
              </a>
            </li>
            <li>
              <a href="#" target="">
                <FaInstagramSquare className="instagram" />
              </a>
            </li>
            <li>
              <a href="#" target="_thapa">
                <FaYoutubeSquare className="youtube" />
              </a>
            </li>
          </ul>

          {/* hamburget menu start  */}
          <div className="hamburger-menu">
            <a href="#" onClick={() => setShowMediaIcons(!showMediaIcons)}>
              <GiHamburgerMenu />
            </a>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;

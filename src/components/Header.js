import React, { useState, useEffect } from "react";
import { serverUrl } from "../utils/api";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Navbar from "../components/Navbar";

export default function Header({ shouldDisplay }) {
  const history = useHistory();
  const [count, setCount] = useState(0);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [searchItems, setSearchItems] = useState([]);
  const [searchDisplayItems, setSearchDisplayItems] = useState([]);
  const [currency, setCurrency] = useState("USD");
  //code for currency change everywhere
  const [aud, setAud] = useState("");
  const [cad, setCad] = useState("");
  //code for currency change everywhere

  const handleSearchQuery = async (query) => {
    var products = [];
    console.log("Header search bar products", products);
    searchItems.forEach((elem) => {
      if (elem.productName.toLowerCase().includes(query.toLowerCase()))
        products.push(elem);
    });
    console.log(products);
    console.log("Header search bar products", products);
    setSearchDisplayItems(products);
    if (!query) {
      setSearchDisplayItems([]);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    setIsLoggedIn(false);
    history.push("/");
  };

  const handleCurrencyChange = (value) => {
    console.log(value);
    setCurrency(value);
    localStorage.setItem("currency", value);
    window.location.reload(true);
  };

  useEffect(() => {
    setCount(window.localStorage.getItem("itemCount"));
    if (localStorage.getItem("token")) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }

    setCurrency(localStorage.getItem("currency"));
    //Fetching products for searchbar
    const data = { queryType: "searchBarOnly" };
    axios
      .post(`${serverUrl}/api/products/search/products`, data)
      .then((res) => {
        console.log("header", res.data.data);
        setSearchItems(res.data.data);
      })
      .catch(() => {
        //handle error
      });
  }, []);

  useEffect(() => {
    //code for currency change everywhere
    console.log("ssaasdasd");
    axios
      .get(
        "https://v6.exchangerate-api.com/v6/50c1270a5b5fef48bfb29271/latest/USD"
      )
      .then((result) => {
        localStorage.setItem("AUD", result.data.conversion_rates.AUD);
        localStorage.setItem("EUR", result.data.conversion_rates.EUR);
        setCurrency(localStorage.getItem("currency"));
        console.log(result.data, "aaa");
      })
      .catch((err) => {
        //handle error
        console.log(err);
      });
  }, []);

  const loginCondition = isLoggedIn ? (
    <>
      <li>
        <Link to="/dashboard">
          <a href data-toggle="modal">
            Dashboard
          </a>
        </Link>
      </li>
      <li onClick={handleLogout}>
        <a href data-toggle="modal">
          Logout
        </a>
      </li>
    </>
  ) : (
    <li>
      <Link to="/account">
        <a href data-toggle="modal" data-target="#login-modal">
          Login
        </a>
      </Link>
    </li>
  );
  const searchBarProducts = searchDisplayItems.length
    ? searchDisplayItems.map((item, index) => (
        <Link to={`/product?id=${item._id}`}>
          <li>
            <img src={item.image} /> <span>{item.productName}</span>
          </li>
        </Link>
      ))
    : null;

  return (
    <div>
      <>
        {/* Start header section */}
        <header id="aa-header">
          {/* start header top  */}
          <div className="aa-header-top">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="aa-header-top-area">
                    {/* start header top left */}
                    <div className="aa-header-top-left">
                      {/* start language */}
                      {shouldDisplay ? (
                        <div className="aa-language">
                          <select
                            onChange={(e) =>
                              handleCurrencyChange(e.target.value)
                            }
                            value={currency}
                          >
                            <option value="USD">USD</option>
                            <option value="AUD">AUD</option>
                            <option value="EUR">EUR</option>
                          </select>
                        </div>
                      ) : null}
                      {/* <div className="aa-language">
                        <select
                          onChange={(e) => handleCurrencyChange(e.target.value)}
                          value={currency}
                        >
                          <option value="USD">USD</option>
                          <option value="AUD">AUD</option>
                          <option value="EUR">EUR</option>
                        </select>
                      </div> */}
                      {/* / language */}
                      {/* start currency */}

                      {/* / currency */}
                      {/* start cellphone */}
                      <div className="cellphone hidden-xs">
                        <p>
                          <span className="fa fa-phone" />
                          00-62-658-658
                        </p>
                      </div>
                      {/* / cellphone */}
                    </div>
                    {/* / header top left */}
                    <div className="aa-header-top-right">
                      <ul className="aa-head-top-nav-right">
                        {/* <li>
                          <Link href="/account">
                            <a>My Account</a>
                          </Link>
                        </li> */}

                        <li className="hidden-xs">
                          <Link to="/cart">My Cart</Link>
                        </li>
                        <li className="hidden-xs">
                          <Link to="/checkout"> Checkout</Link>
                        </li>

                        {loginCondition}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* / header top  */}
          {/* start header bottom  */}
          <div className="aa-header-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="aa-header-bottom-area">
                    {/* logo  */}
                    <div
                      className="aa-logo"
                      style={{ color: " #4618ac !important" }}
                    >
                      {/* Text based logo */}
                      <a>
                        <span className="fa fa-shopping-cart" />
                        <p>
                          <Link to="/">KeyKes </Link>
                          <strong></strong> <span>Coupon Store</span>{" "}
                        </p>
                      </a>
                      {/* img based logo */}
                      {/* <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> */}
                    </div>
                    {/* / logo  */}
                    {/* cart box */}
                    <div className="aa-cartbox">
                      <Link to="/cart">
                        <a className="aa-cart-link">
                          <span className="fa fa-shopping-basket" />
                          <span className="aa-cart-title">SHOPPING CART</span>
                          {/* <span className="aa-cart-notify"></span> */}
                        </a>
                      </Link>
                    </div>
                    {/* / cart box */}
                    {/* search box */}
                    <div className="aa-search-box">
                      <input
                        type="text"
                        name
                        id
                        onChange={(e) => handleSearchQuery(e.target.value)}
                        placeholder="Search here ex. 'Netflix' "
                      />

                      <ul
                        className="search__items"
                        onClick={() => setSearchDisplayItems([])}
                      >
                        {searchBarProducts}
                      </ul>

                      <button>
                        <span className="fa fa-search" />
                      </button>
                    </div>

                    {/* / search box */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* / header bottom  */}
        </header>
        {/* / header section */}
        {/* menu */}

        {/* <section
id="menu"
style={{
  position: "sticky",
  top: "0",
  zIndex: "999",
}}
>
        <Navbar  expand="lg">
  <Container>
   <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="me-auto   " style={{
        letterSpacing:"1px",
        fontSize:"15px",
      
        
}} >
   
        <Nav.Link href="/" >Home</Nav.Link>
     
        <NavDropdown title="Category" id="basic-nav-dropdown">
          <NavDropdown.Item href="">ms-office</NavDropdown.Item>
          <NavDropdown.Item href="/category?tag=ms-office">windows</NavDropdown.Item>
          <NavDropdown.Item href="/category?tag=amazon">amazon</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/category?tag=netflix">Netflix</NavDropdown.Item>
        </NavDropdown>
        <Nav.Link href="#link">Link</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>
</section> */}

        <Navbar />
      </>
    </div>
  );
}

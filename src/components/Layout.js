import React, { useState, useEffect } from "react";
import Header from "../components/Header";

import Footer from "../components/Footer";
export default function Layout({ children }) {
  const [isLogged, setIsLogged] = useState(false);

  return (
    <div>
      <div>{children}</div>

      <Footer />
    </div>
  );
}

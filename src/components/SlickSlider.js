import React from "react";
import Carousel from "react-elastic-carousel";
import Item from "../components/Item";

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 },
];

export default function SlickSlider() {
  return (
    <div>
      <Carousel itemsToShow={1}>
        <Item>
          <img
            src="./netflix.jpg"
            style={{
              height: "65vh",
              width: "80vw",
              visibility: "visible",
              marginTop: "10px",
              objectFit: "cover",
            }}
          />
        </Item>
        <Item>
          <img
            src="./netflix.jpg"
            style={{
              height: "65vh",
              width: "80vw",
              visibility: "visible",
              marginTop: "10px",
              objectFit: "cover",
            }}
          />
        </Item>
        <Item>
          <img
            src="./netflix.jpg"
            style={{
              height: "65vh",
              width: "80vw",
              visibility: "visible",
              marginTop: "10px",
              objectFit: "cover",
            }}
          />
        </Item>
        <Item>
          <img
            src="./netflix.jpg"
            style={{
              height: "65vh",
              width: "80vw",
              visibility: "visible",
              marginTop: "10px",
              objectFit: "cover",
            }}
          />
        </Item>
      </Carousel>
    </div>
  );
}

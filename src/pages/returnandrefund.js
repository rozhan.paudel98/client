import Header from "../components/Header";
import Footer from "../components/Footer";
export default function Refund() {
  return (
    <>
      <Header />
      <div>
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>Return and Refund Policy</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Account</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <div
          style={{
            display: " flex",
            flexDirection: "column",
            alignItems: "left",
            justifyContent: "center",

            alignItem: "center",
            width: "70vw",
            textAlign: "left",
            marginLeft: "15vw",
            marginBottom: "10vh",
          }}
        >
          <br />
          <br />
          <h4>
            Last updated: September 14, 2021
            <br />
            <br />
            <br />
            Thank you for shopping at Keykes.
            <br />
            <br />
            If, for any reason, You are not completely satisfied with a purchase
            We invite You to review our policy on refunds and returns. This
            Return and Refund Policy has been created with the help of the
            Return and Refund Policy Generator.
            <br />
            The following terms are applicable for any products that You
            purchased with Us.
            <br />
            <br />
          </h4>
          <h1>Interpretation and Definitions</h1>
          <br />
          <br />
          <h2>Interpretation</h2>
          <br />
          <h4>
            The words of which the initial letter is capitalized have meanings
            defined under the following conditions. The following definitions
            shall have the same meaning regardless of whether they appear in
            singular or in plural.
          </h4>
          <br />
          <h2>Definitions</h2>
          <h4>
            For the purposes of this Return and Refund Policy: • Company
            (referred to as either "the Company", "We", "Us" or "Our" in this
            Agreement) refers to Keykes.
            <br />
            • Goods refer to the items offered for sale on the Service.
            <br />
            • Orders mean a request by You to purchase Goods from Us.
            <br />
            • Service refers to the Website.
            <br />
            • Website refers to Keykes, accessible from keykes.com
            <br />
            • You means the individual accessing or using the Service, or the
            company, or other legal entity on behalf of which such individual is
            accessing or using the Service, as applicable.
            <br />
          </h4>
          <br />
          <br />
          <h2>Your Order Cancellation Rights</h2>
          <h4>
            You are entitled to cancel Your Order within 7 days without giving
            any reason for doing so. The deadline for cancelling an Order is 7
            days from the date on which You received the Goods or on which a
            third party you have appointed, who is not the carrier, takes
            possession of the product delivered. In order to exercise Your right
            of cancellation, You must inform Us of your decision by means of a
            clear statement. You can inform us of your decision by:
            <br />
            • By email: service@keykes.com <br />
            We will reimburse You no later than 7 days from the day on which We
            receive the returned Goods. We will use the same means of payment as
            You used for the Order, and You will not incur any fees for such
            reimbursement.
          </h4>
          <br />
          <br />
          <h2>Conditions for Returns</h2>
          <h4>
            In order for the Goods to be eligible for a return, please make sure
            that:
            <br />
            • The Goods were purchased in the last 7 days
            <br />
            • The key/account is not used. <br />
            • Only return is possible when you face error or key/account doesn’t
            work. <br />
            The following Goods cannot be returned:
            <br />
            • The supply of Goods made to Your specifications or clearly
            personalized.
            <br />
            • The supply of Goods which according to their nature are not
            suitable to be returned, deteriorate rapidly or where the date of
            expiry is over.
            <br />
            • The supply of Goods which are not suitable for return due to
            health protection or hygiene reasons and were unsealed after
            delivery
            <br />. • The supply of Goods which are, after delivery, according
            to their nature, inseparably mixed with other items.
            <br />
            • The good/key already used and worked. <br />
            • The supply of good not suitable to return if buyer doesn’t read
            the description of product/service well. <br />
            • The reason for cancellation goes against the description of
            product. <br />
            We reserve the right to refuse returns of any merchandise that does
            not meet the above return conditions in our sole discretion. <br />
            Only regular priced Goods may be refunded. Unfortunately, Goods on
            sale cannot be refunded. This exclusion may not apply to You if it
            is not permitted by applicable law. <br />
          </h4>
          <br />
          <br />

          <h2>Gifts</h2>
          <h4>
            If the Goods were marked as a gift when purchased and then shipped
            directly to you, You'll receive a gift credit for the value of your
            return. Once the returned product is received, a gift certificate
            will be mailed to You.
            <br />
            If the Goods weren't marked as a gift when purchased, or the gift
            giver had the Order shipped to themselves to give it to You later,
            We will send the refund to the gift giver.
          </h4>
          <br />
          <br />

          <h2>Contact Us</h2>
          <h4>
            If you have any questions about our Returns and Refunds Policy,
            please contact us: <br />
            • By email: service@keykes.com
            <br />
            Disclaimer: There is no such return policy for cash however in
            return acceptable case we will only return the same value voucher
            code for your next purchase from us. <br />
          </h4>
          <br />
          <br />
        </div>
      </div>
      <Footer />
    </>
  );
}

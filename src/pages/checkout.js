import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import { serverUrl } from "../utils/api";

import { toast } from "react-toastify";

export default function Checkout() {
  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState(0);
  const [ids, setIds] = useState("");
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  const calculateTotal = (items) => {
    var totalValue = 0;
    items.forEach((item) => {
      totalValue = totalValue + parseInt(item.price.productPrice);
    });
    setTotal(totalValue);
  };

  useEffect(() => {
    if (!sessionStorage.getItem("cartItems")) {
      sessionStorage.setItem("cartItems", "[]");
    }
    setCartItems(JSON.parse(sessionStorage.getItem("cartItems")));
    calculateTotal(JSON.parse(sessionStorage.getItem("cartItems")));
    convertProductIds(JSON.parse(sessionStorage.getItem("cartItems")));
    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  const convertProductIds = (items) => {
    var idString = "";
    items.forEach((item) => {
      idString = item._id + "-" + idString;
    });
    console.log(idString);
    setIds(idString);
  };

  const handlePayment = () => {
    return `${serverUrl}/api/paypal/payment/pay`;
  };

  return (
    <div>
      <Header shouldDisplay={true} />
      <section id="checkout">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="checkout-area">
                <form method="POST" action={handlePayment()} target="_blank">
                  <div className="row">
                    <div className="col-md-8">
                      <div className="checkout-left">
                        <div className="panel-group" id="accordion">
                          <h4 style={{ color: "green" }}>Order Summary</h4>
                          <div className="aa-order-summary-area">
                            <table className="table table-responsive">
                              <thead>
                                <tr>
                                  <th>Product</th>
                                  <th>Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                {cartItems
                                  ? cartItems.map((item, i) => {
                                      return (
                                        <>
                                          <tr>
                                            <td>{item.productName}</td>
                                            <td>
                                              {currencyIndex == "EUR"
                                                ? "EUR " +
                                                  (
                                                    item.price.productPrice *
                                                    EUR
                                                  ).toFixed(4)
                                                : currencyIndex == "AUD"
                                                ? "AUD " +
                                                  (
                                                    item.price.productPrice *
                                                    AUD
                                                  ).toFixed(4)
                                                : "USD " +
                                                  item.price.productPrice}
                                            </td>
                                          </tr>
                                        </>
                                      );
                                    })
                                  : null}
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th>Tax</th>
                                  <td>$0</td>
                                </tr>
                                <tr>
                                  <th>Total</th>
                                  <td>
                                    {" "}
                                    {currencyIndex == "EUR"
                                      ? "EUR " + (total * EUR).toFixed(4)
                                      : currencyIndex == "AUD"
                                      ? "AUD " + (total * AUD).toFixed(4)
                                      : "USD " + total}
                                  </td>
                                </tr>
                              </tfoot>
                            </table>
                            <h3>Have a Coupon?</h3>
                            <input
                              className="form-control"
                              placeholder="Enter Coupon Code"
                            ></input>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="checkout-right">
                        <h4>
                          Enter your email where you wanted to receive your
                          product license
                        </h4>
                        <input
                          type="text"
                          name="product_ids"
                          value={ids}
                          style={{ display: "none" }}
                        />

                        <input
                          type="email"
                          placeholder="Destination Email Address"
                          className="form-control w-80"
                          name="email"
                          required={true}
                        ></input>
                        <h4>Payment Method</h4>
                        <div className="aa-payment-method">
                          <label htmlFor="paypal">
                            <input
                              type="radio"
                              id="paypal"
                              name="optionsRadios"
                              defaultChecked
                            />{" "}
                            Via Paypal{" "}
                          </label>
                          <img
                            src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg"
                            border={0}
                            alt="PayPal Acceptance Mark"
                          />
                          <button
                            type="submit"
                            className="aa-browse-btn"
                            disabled={total == 0 ? true : false}
                          >
                            Place Order
                          </button>
                          <p>
                            If total price is zero then place Order button won't
                            work
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

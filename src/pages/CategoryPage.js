import React, { Component, useState, useEffect } from "react";

import axios from "axios";
import { Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../components/Header";
import { serverUrl } from "../utils/api";

const CategoryPage = (props) => {
  const [products, setProducts] = useState([]);

  const [query, setQuery] = useState(props.location.search.split("=")[1]);
  const [cartItems, setCartItems] = useState([]);
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  const handleCart = async (product) => {
    await setCartItems([...cartItems, product]);
    console.log(cartItems);
    toast.success("Product added to cart");
    props.history.push("/cart");
  };

  //Component Did Mount
  useEffect(() => {
    const prevItems = localStorage.getItem("cartItems");
    console.log(prevItems);

    sessionStorage.setItem("cartItems", prevItems);
    setCartItems(JSON.parse(prevItems));

    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  //Unique Items here
  useEffect(() => {
    let cartThings = [];
    if (cartItems) cartThings = getUniqueCartItems();

    sessionStorage.setItem("cartItems", JSON.stringify(cartThings));
    localStorage.setItem("itemCount", cartThings.length);
  }, [cartItems]);

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  const getUniqueCartItems = () => {
    const unique = cartItems.filter(onlyUnique);
    return unique;
  };
  useEffect(() => {
    axios
      .post(`${serverUrl}/api/products/fetch/products/tag`, {
        tag: props.location.search.split("=")[1],
      })
      .then((result) => {
        setProducts(result.data.data);
        setQuery(props.location.search.split("=")[1]);
      });
  }, []);

  setTimeout(() => {
    setQuery(props.location.search.split("=")[1]);
  }, 1000);

  useEffect(() => {
    axios
      .post(`${serverUrl}/api/products/fetch/products/tag`, {
        tag: props.location.search.split("=")[1],
      })
      .then((result) => {
        setProducts(result.data.data);
      });
  }, [query]);

  return (
    <div>
      <Header shouldDisplay={true} />
      <section>
        <ToastContainer />
        <div className="container__1" style={{ height: "100vh" }}>
          <div className="row">
            <div className="col-md-12">
              <div
                className="checkout-area"
                style={{
                  marginLeft: "20vw",
                }}
              >
                <h3>Products</h3>
                <div style={{ display: "inline", marginRight: "10vw" }}>
                  {products.map((product, index) => {
                    return (
                      <div class="content__card1 col-sm-4 col-lg-3">
                        <Link to={`/product?id=${product._id}`}>
                          <img src={product.image} />
                        </Link>
                        <h3>{product.productName}</h3>
                        <p>{product.description}</p>
                        <h6>
                          {currencyIndex == "EUR"
                            ? "EUR" + product.price.productPrice * EUR
                            : currencyIndex == "AUD"
                            ? "AUD" + product.price.productPrice * AUD
                            : "USD" + product.price.productPrice}
                        </h6>
                        <ul>
                          <li>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </li>
                          <li>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </li>
                          <li>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </li>
                          <li>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </li>
                          <li>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </li>
                        </ul>
                        <button
                          class="buy-2"
                          onClick={() => handleCart(product)}
                        >
                          Buy Now
                        </button>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default CategoryPage;

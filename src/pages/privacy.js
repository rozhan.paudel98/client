import Header from "../components/Header";
import Footer from "../components/Footer";
export default function Privacy() {
  return (
    <>
      <Header />
      <div
        style={{
          display: " flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",

          alignItem: "center",
          width: "70vw",
          textAlign: "left",
          marginLeft: "15vw",
          marginBottom: "10vh",
        }}
      >
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>Privacy and Policy</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Account</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <div
          className="about-section"
          style={{
            display: " flex",
            flexDirection: "column",
            alignItems: "left",
            justifyContent: "center",

            width: "70vw",
            textAlign: "left",

            marginBottom: "10vh",
          }}
        >
          <h1>
            <strong>Privacy Policy for Keykes</strong>
          </h1>
          <h4>
            At keykes, accessible from keykes.com, one of our main priorities is
            the privacy of our visitors. This Privacy Policy document contains
            types of information that is collected and recorded by keykes and
            how we use it.
          </h4>
          <h4>
            If you have additional questions or require more information about
            our Privacy Policy, do not hesitate to contact us.
          </h4>
          <h4>
            This Privacy Policy applies only to our online activities and is
            valid for visitors to our website with regards to the information
            that they shared and/or collect in keykes. This policy is not
            applicable to any information collected offline or via channels
            other than this website. Our Privacy Policy was created with the
            help of the Free Privacy Policy Generator.
          </h4>
          <h2>Consent</h2>
          <h4>
            By using our website, you hereby consent to our Privacy Policy and
            agree to its terms.
          </h4>
          <h4>Information we collect </h4>
          <h4>
            The personal information that you are asked to provide, and the
            reasons why you are asked to provide it, will be made clear to you
            at the point we ask you to provide your personal information.
          </h4>
          <h4>
            If you contact us directly, we may receive additional information
            about you such as your name, email address, phone number, the
            contents of the message and/or attachments you may send us, and any
            other information you may choose to provide
          </h4>
          <h4>
            When you register for an Account, we may ask for your contact
            information, including items such as name, company name, address,
            email address, and telephone number.
          </h4>
          <h4>
            We use the information we collect in various ways, including to:
            <ul style={{ listStyleType: "circle" }}>
              <li>1. Provide, operate, and maintain our website</li>
              <li>2. Improve, personalize, and expand our website</li>
              <li>3. Understand and analyze how you use our website</li>
              <li>
                4 .Develop new products, services, features, and functionality
              </li>
              <li>
                5. Communicate with you, either directly or through one of our
                partners, including for customer service, to provide you with
                updates and other information relating to the website, and for
                marketing and promotional purposes
              </li>
              <li>6. Send you emails</li>
              <li>7. Find and prevent fraud</li>
            </ul>
          </h4>
          <h2> Log Files </h2>
          <h4>
            keykes follows a standard procedure of using log files. These files
            log visitors when they visit websites. All hosting companies do this
            and a part of hosting services' analytics. The information collected
            by log files include internet protocol (IP) addresses, browser type,
            Internet Service Provider (ISP), date and time stamp, referring/exit
            pages, and possibly the number of clicks. These are not linked to
            any information that is personally identifiable. The purpose of the
            information is for analyzing trends, administering the site,
            tracking users' movement on the website, and gathering demographic
            information.
          </h4>
          <h4>Google DoubleClick DART Cookie</h4>
          <h4>
            Google is one of a third-party vendor on our site. It also uses
            cookies, known as DART cookies, to serve ads to our site visitors
            based upon their visit to www.website.com and other sites on the
            internet. However, visitors may choose to decline the use of DART
            cookies by visiting the Google ad and content network Privacy Policy
            at the following{" "}
            <a href="https://policies.google.com/technologies/ads"></a>URL
          </h4>
          <h4>Advertising Partners Privacy Policies</h4>
          <h4>
            You may consult this list to find the Privacy Policy for each of the
            advertising partners of keykes. <br />
            Third-party ad servers or ad networks uses technologies like
            cookies, JavaScript, or Web Beacons that are used in their
            respective advertisements and links that appear on keykes, which are
            sent directly to users' browser. They automatically receive your IP
            address when this occurs. These technologies are used to measure the
            effectiveness of their advertising campaigns and/or to personalize
            the advertising content that you see on websites that you visit.
          </h4>
          <h4>
            <b>
              Note that keykes has no access to or control over these cookies
              that are used by third-party advertisers.
            </b>
          </h4>
          <h4>Third Party Privacy Policies</h4>
          <h4>
            keykes's Privacy Policy does not apply to other advertisers or
            websites. Thus, we are advising you to consult the respective
            Privacy Policies of these third-party ad servers for more detailed
            information. It may include their practices and instructions about
            how to opt-out of certain options.
          </h4>{" "}
          <br />
          <h4>
            You can choose to disable cookies through your individual browser
            options. To know more detailed information about cookie management
            with specific web browsers, it can be found at the browsers'
            respective websites.
          </h4>
          <h4>CCPA Privacy Rights (Do Not Sell My Personal Information)</h4>
          <h4>
            Under the CCPA, among other rights, California consumers have the
            right to: Request that a business that collects a consumer's
            personal data disclose the categories and specific pieces of
            personal data that a business has collected about consumers.
            <br />
            Request that a business delete any personal data about the consumer
            that a business has collected.
            <br />
            Request that a business that sells a consumer's personal data, not
            sell the consumer's personal data.
            <br />
            If you make a request, we have one month to respond to you. If you
            would like to exercise any of these rights, please contact us.
            <br />
          </h4>
          <h2>GDPR Data Protection Rights</h2>
          <h4>
            We would like to make sure you are fully aware of all of your data
            protection rights. Every user is entitled to the following:
            <br />
            The right to access – You have the right to request copies of your
            personal data. We may charge you a small fee for this service.
            <br />
            The right to rectification – You have the right to request that we
            correct any information you believe is inaccurate. You also have the
            right to request that we complete the information you believe is
            incomplete.
            <br />
            The right to erasure – You have the right to request that we erase
            your personal data, under certain conditions.
            <br />
            The right to restrict processing – You have the right to request
            that we restrict the processing of your personal data, under certain
            conditions.
            <br />
            The right to object to processing – You have the right to object to
            our processing of your personal data, under certain conditions.
            <br />
            The right to data portability – You have the right to request that
            we transfer the data that we have collected to another organization,
            or directly to you, under certain conditions.
            <br />
            If you make a request, we have one month to respond to you. If you
            would like to exercise any of these rights, please contact us.
            <br />
          </h4>
          <h2>Children's Information</h2>
          <h4>
            {" "}
            Another part of our priority is adding protection for children while
            using the internet. We encourage parents and guardians to observe,
            participate in, and/or monitor and guide their online activity.
            <br />
            keykes does not knowingly collect any Personal Identifiable
            Information from children under the age of 13. If you think that
            your child provided this kind of information on our website, we
            strongly encourage you to contact us immediately and we will do our
            best efforts to promptly remove such information from our records.
            <br />
          </h4>
        </div>
      </div>
      <Footer />
    </>
  );
}

import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import FeaturedProductSection from "../components/FeaturedProductSection";
import Footer from "../components/Footer";
import Header from "../components/Header";
import SlickSlider from "../components/SlickSlider";
import { useHistory } from "react-router-dom";
import ProductSection from "../components/ProductSection";
import RecentProductSection from "../components/RecentProductSection";
import { ToastContainer, toast } from "react-toastify";
import { serverUrl } from "../utils/api";

export default function HomePage() {
  const history = useHistory();

  const [currency, setCurrency] = useState();
  const [aud, setAud] = useState("");
  const [cad, setCad] = useState("");
  const [cartItems, setCartItems] = useState([]);
  const [products, setProducts] = useState([]);
  const [fproducts, setFroducts] = useState([]);
  const [newProducts, setNewProducts] = useState([]);

  var prevItems = localStorage.getItem("cartItems");
  console.log(prevItems);

  if (!prevItems) localStorage.setItem("cartItems", "[]");

  const handleCart = async (product) => {
    await setCartItems([...cartItems, product]);
    toast.info("Product added to cart ! ");
  };

  //Component Did Mount
  useEffect(() => {
    prevItems = localStorage.getItem("cartItems");

    if (!prevItems.length) localStorage.setItem("cartItems", "[]");
    setCartItems(JSON.parse(prevItems));
  }, []);

  //Unique Items here
  useEffect(() => {
    let cartThings = [];
    if (cartItems) cartThings = getUniqueCartItems();

    sessionStorage.setItem("cartItems", JSON.stringify(cartThings));
  }, [cartItems]);

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  const getUniqueCartItems = () => {
    const unique = cartItems.filter(onlyUnique);
    return unique;
  };

  useEffect(() => {
    axios
      .get(
        "https://v6.exchangerate-api.com/v6/50c1270a5b5fef48bfb29271/latest/USD"
      )
      .then((result) => {
        localStorage.setItem("AUD", result.data.conversion_rates.AUD);
        localStorage.setItem("EUR", result.data.conversion_rates.EUR);
        setCurrency(localStorage.getItem("currency"));
      })
      .catch(() => {
        //handle error
      });

    //pachi add gareko
  }, []);

  useEffect(() => {
    history.push("/");
  }, [currency]);

  useEffect(() => {
    axios.get(`${serverUrl}/api/products/fetch/all`).then((result) => {
      console.log(result.data.data);
      const promotedProducts = result.data.data.filter(
        (product) => product.isPromoted == true
      );
      setFroducts(promotedProducts);
      setProducts(result.data.data);

      setNewProducts([...result.data.data].reverse());
    });
  }, []);
  return (
    <div>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Helmet application" />
        <script type="text/javascript" src="js/nouislider.js" defer></script>
        <script
          src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"
          defer
        ></script>

        <script src="js/bootstrap.js" defer></script>

        <script
          type="text/javascript"
          src="js/jquery.smartmenus.js"
          defer
        ></script>

        <script
          type="text/javascript"
          src="js/jquery.smartmenus.bootstrap.js"
          defer
        ></script>

        <script
          type="text/javascript"
          src="js/jquery.simpleGallery.js"
          defer
        ></script>
        <script
          type="text/javascript"
          src="js/jquery.simpleLens.js"
          defer
        ></script>

        <script type="text/javascript" src="js/nouislider.js" defer></script>

        <script src="js/custom.js" defer></script>
      </Helmet>
      <Header shouldDisplay={true} />
      <SlickSlider />
      <FeaturedProductSection productsItems={fproducts} cartfunc={handleCart} />
      <ProductSection productsItems={products} cartfunc={handleCart} />
      <RecentProductSection productsItems={newProducts} cartfunc={handleCart} />
      <Footer />
    </div>
  );
}

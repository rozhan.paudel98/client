import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../components/Header";
import { useHistory } from "react-router-dom";
import Footer from "../components/Footer";
import { serverUrl } from "../utils/api";

export default function Account(props) {
  const history = useHistory();
  const [emailL, setEmailL] = useState("");
  const [passwordL, setPasswordL] = useState("");

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(`${serverUrl}/api/users/login`, {
        email: emailL,
        password: passwordL,
      })
      .then((result) => {
        console.log(result);
        if (!result.data.isAuth) {
          toast.error(result.data.msg);
        }

        if (result.data.isAuth) {
          console.log(result.data);
          localStorage.setItem("token", result.data.token);
          localStorage.setItem("email", result.data.email);
          toast.success("Login Success !");
          props.history.push("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleRegister = (e) => {
    e.preventDefault();
    axios
      .post(`${serverUrl}/api/users/register`, {
        name: name,
        email: email,
        password: password,
      })
      .then((result) => {
        if (!result.data.success) {
          toast.error(result.data.error);
        }

        if (result.data.success) {
          toast.success("Register Successfull ! Please Login to Continue ");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <Header shouldDisplay={false} />
      <div>
        {/* catg header banner section */}
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>Account Page</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Account</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        {/* / catg header banner section */}
        {/* Cart view section */}
        <section id="aa-myaccount">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="aa-myaccount-area">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="aa-myaccount-login">
                        <h4>Login</h4>
                        <form
                          onSubmit={(e) => handleSubmit(e)}
                          className="aa-login-form"
                        >
                          <label htmlFor>
                            Email address<span>*</span>
                          </label>
                          <input
                            type="text"
                            placeholder=" email"
                            onChange={(e) => setEmailL(e.target.value)}
                          />
                          <label htmlFor>
                            Password<span>*</span>
                          </label>
                          <input
                            type="password"
                            placeholder="Password"
                            onChange={(e) => setPasswordL(e.target.value)}
                          />
                          <button type="submit" className="aa-browse-btn">
                            Login
                          </button>
                          <label className="rememberme" htmlFor="rememberme">
                            <input type="checkbox" id="rememberme" /> Remember
                            me{" "}
                          </label>
                          <p className="aa-lost-password">
                            <a href="#">Lost your password?</a>
                          </p>
                        </form>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="aa-myaccount-register">
                        <h4>Register</h4>
                        <form
                          onSubmit={(e) => handleRegister(e)}
                          className="aa-login-form"
                        >
                          <label htmlFor>
                            Full Name <span>*</span>
                          </label>
                          <input
                            type="text"
                            placeholder="Name "
                            onChange={(e) => setName(e.target.value)}
                            required={true}
                          />
                          <label htmlFor>
                            Email address<span>*</span>
                          </label>
                          <input
                            type="text"
                            placeholder="email"
                            onChange={(e) => setEmail(e.target.value)}
                            required={true}
                          />
                          <label htmlFor>
                            Password<span>*</span>
                          </label>
                          <input
                            type="password"
                            placeholder="Password"
                            required={true}
                            onChange={(e) => setPassword(e.target.value)}
                          />
                          <button type="submit" className="aa-browse-btn">
                            Register
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* / Cart view section */}
      </div>
      <Footer />
      <ToastContainer />
      {/* Same as */}
      <ToastContainer />
    </>
  );
}

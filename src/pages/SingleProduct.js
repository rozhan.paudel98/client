import { useEffect, useState } from "react";
import { serverUrl } from "../utils/api";

import { Link } from "react-router-dom";
import axios from "axios";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../components/Header";

export default function SingleProduct(props) {
  const id = props.location.search.split("=")[1];
  var prevItems = !localStorage.getItem("cartItems")
    ? sessionStorage.getItem("cartItems")
    : localStorage.getItem("cartItems");
  if (!prevItems) {
    localStorage.setItem("cartItems", "[]");
    sessionStorage.setItem("cartItems", "[]");
  }
  const [product, setProduct] = useState("");
  const [productId, setProductId] = useState(props.match.params.id);
  const [price, setPrice] = useState("");
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  //cart handler
  const [cartItems, setCartItems] = useState([]);
  const handleCart = async (product) => {
    await setCartItems([...cartItems, product]);

    toast.success("Product added to cart");
    props.history.push("/cart");
  };

  //Component Did Mount
  useEffect(() => {
    prevItems = !localStorage.getItem("cartItems")
      ? sessionStorage.getItem("cartItems")
      : localStorage.getItem("cartItems");

    setCartItems(JSON.parse(prevItems));
    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  //Unique Items here
  useEffect(() => {
    let cartThings = [];
    if (cartItems) cartThings = getUniqueCartItems();

    sessionStorage.setItem("cartItems", JSON.stringify(cartThings));
  }, [cartItems]);

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  const getUniqueCartItems = () => {
    const unique = cartItems.filter(onlyUnique);
    return unique;
  };
  //cart handler

  useEffect(() => {
    axios
      .get(`${serverUrl}/api/products/product/${id}`)
      .then((result) => {
        setProduct(result.data.data);
        setPrice(result.data.data.price.productPrice);
        setProductId(id);
      })
      .catch((err) => {
        //handle error here
      });
  }, [id]);

  return (
    <div>
      <div>
        <ToastContainer />
        <Header shouldDisplay={false} />
        <section id="aa-product-details">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="aa-product-details-area">
                  <div className="aa-product-details-content">
                    <div className="row">
                      <div className="col-md-5 col-sm-5 col-xs-12">
                        <div className="aa-product-view-slider">
                          <div
                            id="demo-1"
                            className="simpleLens-gallery-container"
                          >
                            <div className="simpleLens-container">
                              <div className="simpleLens-big-image-container">
                                <a
                                  data-lens-image="img/view-slider/large/polo-shirt-1.png"
                                  className="simpleLens-lens-image"
                                >
                                  <img
                                    src={product.image}
                                    style={{ objectFit: "fill" }}
                                    className="simpleLens-big-image"
                                  />
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* Modal view content */}
                      <div className="col-md-7 col-sm-7 col-xs-12">
                        <div className="aa-product-view-content">
                          <h3>{product.productName} </h3>

                          <div className="aa-price-block">
                            <span>
                              {currencyIndex == "EUR"
                                ? "EUR " + (price * EUR).toFixed(4)
                                : currencyIndex == "AUD"
                                ? "AUD " + (price * AUD).toFixed(4)
                                : "USD " + price}
                            </span>
                          </div>
                          <p>{product.description}</p>

                          <div className="aa-prod-quantity">
                            <p className="aa-prod-category">
                              Category: <b>{product.tag} </b>
                            </p>
                          </div>
                          <div className="aa-prod-view-bottom">
                            <a
                              className="aa-add-to-cart-btn"
                              onClick={() => handleCart(product)}
                              style={{ cursor: "pointer" }}
                            >
                              Buy Now
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="aa-product-details-bottom">
                    <ul className="nav nav-tabs" id="myTab2">
                      <li>
                        <a href="#description" data-toggle="tab">
                          Description
                        </a>
                      </li>
                      <li>
                        <a href="#review" data-toggle="tab">
                          Reviews
                        </a>
                      </li>
                    </ul>

                    <div className="tab-content">
                      <div className="tab-pane fade in active" id="description">
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged. It was popularised in the 1960s with the
                          release of Letraset sheets containing Lorem Ipsum
                          passages, and more recently with desktop publishing
                          software like Aldus PageMaker including versions of
                          Lorem Ipsum.
                        </p>
                        <ul>
                          <li>
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Quod, culpa!
                          </li>
                          <li>Lorem ipsum dolor sit amet.</li>
                          <li>
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit.
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Dolor qui eius esse!
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Quibusdam, modi!
                          </li>
                        </ul>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                          elit. Illum, iusto earum voluptates autem esse
                          molestiae ipsam, atque quam amet similique ducimus
                          aliquid voluptate perferendis, distinctio!
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                          elit. Blanditiis ea, voluptas! Aliquam facere quas
                          cumque rerum dolore impedit, dicta ducimus repellat
                          dignissimos, fugiat, minima quaerat necessitatibus?
                          Optio adipisci ab, obcaecati, porro unde accusantium
                          facilis repudiandae.
                        </p>
                      </div>
                      <div className="tab-pane fade " id="review">
                        <div className="aa-product-review-area">
                          <h4>2 Reviews for T-Shirt</h4>
                          <ul className="aa-review-nav">
                            <li>
                              <div className="media">
                                <div className="media-left">
                                  <a href="#">
                                    <img
                                      className="media-object"
                                      src="img/testimonial-img-3.jpg"
                                      alt="girl image"
                                    />
                                  </a>
                                </div>
                                <div className="media-body">
                                  <h4 className="media-heading">
                                    <strong>Marla Jobs</strong> -{" "}
                                    <span>March 26, 2016</span>
                                  </h4>
                                  <div className="aa-product-rating">
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star-o" />
                                  </div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit.
                                  </p>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div className="media">
                                <div className="media-left">
                                  <a href="#">
                                    <img
                                      className="media-object"
                                      src="img/testimonial-img-3.jpg"
                                      alt="girl image"
                                    />
                                  </a>
                                </div>
                                <div className="media-body">
                                  <h4 className="media-heading">
                                    <strong>Marla Jobs</strong> -{" "}
                                    <span>March 26, 2016</span>
                                  </h4>
                                  <div className="aa-product-rating">
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star" />
                                    <span className="fa fa-star-o" />
                                  </div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit.
                                  </p>
                                </div>
                              </div>
                            </li>
                          </ul>
                          <h4>Add a review</h4>
                          <div className="aa-your-rating">
                            <p>Your Rating</p>
                            <a href="#">
                              <span className="fa fa-star-o" />
                            </a>
                            <a href="#">
                              <span className="fa fa-star-o" />
                            </a>
                            <a href="#">
                              <span className="fa fa-star-o" />
                            </a>
                            <a href="#">
                              <span className="fa fa-star-o" />
                            </a>
                            <a href="#">
                              <span className="fa fa-star-o" />
                            </a>
                          </div>

                          <form action className="aa-review-form">
                            <div className="form-group">
                              <label htmlFor="message">Your Review</label>
                              <textarea
                                className="form-control"
                                rows={3}
                                id="message"
                                defaultValue={""}
                              />
                            </div>
                            <div className="form-group">
                              <label htmlFor="name">Name</label>
                              <input
                                type="text"
                                className="form-control"
                                id="name"
                                placeholder="Name"
                              />
                            </div>
                            <div className="form-group">
                              <label htmlFor="email">Email</label>
                              <input
                                type="email"
                                className="form-control"
                                id="email"
                                placeholder="example@gmail.com"
                              />
                            </div>
                            <button
                              type="submit"
                              className="btn btn-default aa-review-submit"
                            >
                              Submit
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
// import React from 'react'

// export default function SingleProduct(props) {
//   console.log(props.match.params.id)
//   return (
//     <div>
//       Hello
//     </div>
//   )
// }
